import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  // user = {`email`:`test@example.com`}

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.get().subscribe((data: any) => {
      console.log(data);
      // this.user = data;
    });
  }

}
